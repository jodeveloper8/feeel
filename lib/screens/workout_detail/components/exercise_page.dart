// Copyright (C) 2019–2021 Miroslav Mazel
//
// This file is part of Feeel.
//
// Feeel is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version. As an additional permission under
// section 7, you are allowed to distribute the software through an app
// store, even if that store has restrictive terms and conditions that
// are incompatible with the AGPL, provided that the source is also
// available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// Feeel is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Feeel.  If not, see <http://www.gnu.org/licenses/>.

import 'package:feeel/components/exercise_sheet.dart';
import 'package:flutter/services.dart';
import '../../../controllers/workout_controller.dart';
import 'package:feeel/controllers/workout_view.dart';
import 'package:feeel/db/asset_helper.dart';
import 'package:feeel/enums/exercise_type.dart';
import 'package:feeel/enums/workout_stage.dart';
import 'package:feeel/models/view/exercise.dart';
import 'package:feeel/models/view/exercise_step.dart';
import 'package:feeel/models/view/workout.dart';
import 'package:feeel/models/view/workout_exercise.dart';
import 'package:feeel/theming/feeel_shade.dart';
import 'package:feeel/theming/feeel_swatch.dart';
import 'package:feeel/screens/workout_detail/components/exercise_layout.dart';
import 'package:flutter/material.dart';
import 'package:feeel/i18n/translations.dart';

class ExercisePage extends StatefulWidget {
  final Workout workout;
  final FeeelSwatch colorSwatch;
  final WorkoutController workoutController;

  const ExercisePage(
      {Key? key,
      required this.workout,
      required this.colorSwatch,
      required this.workoutController})
      : super(key: key);

  @override
  _ExercisePageState createState() {
    return _ExercisePageState();
  }
}

class _ExercisePageState extends State<ExercisePage> implements WorkoutView {
  bool _paused = false;
  late int _seconds;
  int _exercisePos = 0;
  int _stepPos = 0;
  WorkoutStage _stage = WorkoutStage.BREAK;

  // todo on setup, do _second = widget.workoutExercise.breakBeforeDuration ?

  @override
  void initState() {
    widget.workoutController.setView(this);
    _seconds = widget.workout.countdownDuration;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final themeDarkColor = widget.colorSwatch
        .getColorByBrightness(FeeelShade.DARK, theme.brightness);
    final exercise = _getExercise(_exercisePos);
    final imageSlug = (exercise.steps?[_stepPos].imageSlug != null)
        ? exercise.steps![_stepPos].imageSlug
        : exercise.imageSlug;
    final headOnly = exercise.type == ExerciseType.HEAD;
    return
        // CallbackShortcuts(
        //     //todo doesn't always activate!!!
        //     bindings: {
        //       const SingleActivator(LogicalKeyboardKey.space): () => setState(() {
        //             widget.workoutController.togglePlayPause();
        //           })
        //     },
        //     child:
        GestureDetector(
            child: Material(
                color: theme.backgroundColor,
                child: Column(
                  children: <Widget>[
                    ExerciseHeader(
                        color: themeDarkColor,
                        paused: _paused,
                        counterText: _seconds.toString()),
                    _paused
                        ? Row(children: <Widget>[
                            Expanded(
                                child: IconButton(
                              iconSize: 32,
                              color: themeDarkColor,
                              tooltip: "Previous exercise".i18n,
                              icon: Icon(Icons.skip_previous),
                              onPressed: () {
                                widget.workoutController.skipToPrevious();
                              },
                            )),
                            Expanded(
                              child: IconButton(
                                  iconSize: 64,
                                  color: themeDarkColor,
                                  tooltip: "Resume workout".i18n,
                                  icon: Icon(Icons.play_arrow),
                                  onPressed: () {
                                    widget.workoutController.togglePlayPause();
                                  }),
                            ),
                            Expanded(
                              child: IconButton(
                                  iconSize: 32,
                                  color: themeDarkColor,
                                  tooltip: "Next exercise".i18n,
                                  icon: Icon(
                                    Icons.skip_next,
                                  ),
                                  onPressed: () {
                                    widget.workoutController.skipToNext();
                                  }),
                            )
                          ])
                        : Text(
                            "Tap for controls".i18n,
                            style: TextStyle(color: themeDarkColor),
                          ),
                    Expanded(
                        child: ExerciseLayout(
                      paused: _paused,
                      colorSwatch: widget.colorSwatch,
                      title: exercise.name.i18n,
                      headOnly: headOnly,
                      onBreak: _stage == WorkoutStage.BREAK ||
                          _stage == WorkoutStage.COUNTDOWN,
                      onLearn: () => onLearn(exercise),
                      triangleSeed: headOnly ? exercise.name.hashCode : 0,
                      imageSlug: imageSlug,
                      flipped: exercise.flipped,
                      animated: exercise.animated,
                    )), // todo show drag handle
                  ],
                )),
            onTap: () {
              //todo setstate
              setState(() {
                widget.workoutController.togglePlayPause();
              });
            },
            //todo onHorizontalDrag
            onVerticalDragStart: (_) => onLearn(exercise)); //);
  }

  void onLearn(Exercise exercise) {
    if (!_paused &&
        (_stage == WorkoutStage.BREAK || _stage == WorkoutStage.COUNTDOWN)) {
      widget.workoutController.togglePlayPause();
    }
    if (_paused) {
      ExerciseSheet.showSheet(context, exercise, widget.colorSwatch);
    }
  }

  @override
  void dispose() {
    super.dispose();
    widget.workoutController.clearView();
  }

  @override
  void onStart(int exercisePos, WorkoutExercise nextExercise, int duration) {
    setState(() {
      _seconds = duration;
      _exercisePos = exercisePos;
      _stage = WorkoutStage.BREAK;
    });
  }

  @override
  void onBreak(int exercisePos, WorkoutExercise nextExercise, int duration) {
    setState(() {
      _seconds = duration;
      _exercisePos = exercisePos;
      _stage = WorkoutStage.BREAK;
    });
  }

  @override
  void onExercise(int exercisePos, WorkoutExercise exercise, ExerciseStep? step,
      int duration) {
    setState(() {
      if ((exercise.exercise.steps?.length ?? 0) > 1)
        _preloadUpcomingStep();
      else
        _preloadUpcomingExercise();

      _seconds = duration;
      _exercisePos = exercisePos;
      _stage = WorkoutStage.EXERCISE;
      _stepPos = 0; //todo should I do this for the break too?
    });
  }

  Exercise _getExercise(int i) => widget.workout.workoutExercises[i].exercise;

  void _preloadUpcomingStep() {
    final steps = _getExercise(_exercisePos).steps;
    if (steps != null) {
      final nextImageSlug = (_stepPos + 1 < steps.length)
          ? steps[_stepPos + 1].imageSlug
          : steps[0].imageSlug;
      if (nextImageSlug != null) {
        precacheImage(
            Image.asset(AssetHelper.getImage(nextImageSlug)).image, context);
      }
    }
  }

  void _preloadUpcomingExercise() {
    if (_exercisePos + 1 < widget.workout.workoutExercises.length) {
      final nextImageSlug = _getExercise(_exercisePos + 1).imageSlug;
      if (nextImageSlug != null) {
        precacheImage(
            Image.asset(AssetHelper.getImage(nextImageSlug)).image, context);
      }
    }
  }

  @override
  void onCount(int seconds, _) {
    setState(() {
      _seconds = seconds;
    });
  }

  @override
  void onPause() {
    setState(() {
      _paused = true;
    });
  }

  @override
  void onPlay() {
    setState(() {
      _paused = false;
    });
  }

  @override
  void onLaterStep(int stepPos, ExerciseStep step) {
    setState(() {
      _stepPos = stepPos;
    });
  }

  @override
  void onCountdown(int exercisePos) {
    setState(() {
      _exercisePos = exercisePos;
      _stage = WorkoutStage.COUNTDOWN;
    });
  }
}

class ExerciseHeader extends StatelessWidget {
  final String counterText; //todo is this really the best way to rerender time?
  final Color color;
  final bool paused;

  ExerciseHeader(
      {Key? key,
      required this.color,
      required this.counterText,
      this.paused = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        bottom: false,
        child: Align(
            alignment: Alignment.topCenter,
            child: Stack(
              children: <Widget>[
                if (paused)
                  Align(
                    child: Padding(
                      child: BackButton(
                        color: color,
                      ),
                      padding: EdgeInsets.all(16),
                    ),
                    alignment: AlignmentDirectional.centerStart,
                  ),
                Align(
                    alignment: Alignment.center,
                    child: Padding(
                        padding: EdgeInsets.all(16),
                        child: Text(
                          counterText,
                          style: TextStyle(
                              fontSize: paused ? 24 : 72,
                              fontWeight: FontWeight.w900,
                              color: color),
                        ))),
              ],
            )));
  }
}
