class ExerciseStep {
  final int duration;
  final String? imageSlug;
  final String? voiceHint;

  ExerciseStep({required this.duration, this.imageSlug, this.voiceHint});
}
