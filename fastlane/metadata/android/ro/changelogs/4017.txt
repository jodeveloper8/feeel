O mulțime de lucruri noi, inclusiv:
* Categorii de antrenamente codate pe culori
* Exerciții noi
* Vizualizare specială a imaginilor de exerciții cu un prim-plan al unui cap
* Suport pentru iOS
* Noua traducere în limba arabă + suport RTL
* Traducere nouă în limba croată
* Modificări ale temei
* Bokmal norvegian a fost șters din nou
* Diverse corecturi de erori și actualizări de traducere
